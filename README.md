# pyfilesv

> I just want to let other peoples upload file to my computer and let they see
> my files

Then this is for you.

## Installation

### On the server

The only dependency for this to work is `python3`, seriously.

### On the client

You only need a browser to send and receive file. Hell, even `w3m` and `lynx`
work.

## Usage

### On the server

```
./pyfilesv
```

### On the client

Navigate to the address printed on the server's terminal (or ask for it)


Last, but not least:
```
./pyfilesv --help
```
